# Guide pour les portes ouvertes

## But de ce document

Ce document a pour but de vous guider dans le déploiement d'une nouvelle version de la mini-application de la bulle DevOps des portes ouvertes de l'ETML.

## Introduction de l'activitée proposée

Nous souhaitons initier par la pratique les visiteurs des Portes ouvertes de l'ETML au DevOps.
Pour ce faire, nous allons proposer à chaque visiteur de déployer une nouvelle version d'une mini-application web. 
Comme nous n'avons, bien sûr, pas le temps de faire développer une nouvelle fonctionnalité à chaque visiteur, nous allons lui proposer :
- soit d'ajouter un test unitaire à une fonction php existance.
- soit d'écrire une nouvelle fonction php et un test unitaire associé.

## Installation du projet

### Récupérer le code de l'application

Prérequis : Vous devez avoir installé :
- git
- php
- composer

Pour récupérer le code de l'application, placez vous dans un nouveau répertoire
de travail et taper la commande suivante :

`git clone https://gitlab.com/GregLeBarbar/po-ci`

## Guide de la procédure

### Etape 1 : remplir le formulaire

### Etape 2 : ajouter le fichier de la fonction et le fichier du test unitaire

### Etape 3 : je vérifie en local la qualité du code

`./vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run --diff`

### Etape 4 : je vérifie en local que les tests unitaires passent

`./vendor/bin/phpunit tests`

### Etape 5 : Je pousse les nouveaux fichiers créés

`./vendor/bin/php-cs-fixer fix --allow-risky=yes; git pull; git add .; git commit -m "new entry"; git push`

et voila ! Lorsque CI/CD aura fait son travail on verra le résultat sur la homepage
