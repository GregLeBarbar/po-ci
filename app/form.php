<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>Portes ouvertes ETML</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="../index.php"><img src="img/etml.jpg" style="width: 100px"></a>
                <h1 style="color: grey; font-size:38px">Portes ouvertes - Informatique - bulle DevOps</h1>
            </div>
        </div>
    </header>
    <main role="main">
        <div class="container">
            <h2 class="m-3">Saisir les informations pour un nouveau visiteur</h2>
            <form class="m-3" role="form" method="GET" action="">
                <div class="form-group row">
                    <label for="visitorFirstName" class="col-sm-2 col-form-label">Prénom du visiteur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="visitorFirstName" name="visitorFirstName" placeholder="Prénom du visiteur">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="visitorLastName" class="col-sm-2 col-form-label">Nom du visiteur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="visitorLastName" name="visitorLastName" placeholder="Nom du visiteur">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="studentFirstName" class="col-sm-2 col-form-label">Prénom de l'étudiant</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="studentFirstName" name="studentFirstName" placeholder="Prénom de l'étudiant">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="studentLastName" class="col-sm-2 col-form-label">Nom de l'étudiant</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="studentLastName" name="studentLastName" placeholder="Nom de l'étudiant">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phpFunction" class="col-sm-2 col-form-label">Fonction php</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phpFunction" name="phpFunction" placeholder="Fonction php">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="poDate" class="col-sm-2 col-form-label">Date et Heure</label>
                    <div class="col-sm-10">
                        <input type="datetime-local" class="form-control" id="poDate" name="poDate" placeholder="Date et heure">
                    </div>
                </div>
                <input type="hidden" name="validated" id="validated" value="0">
                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                        <!--<button type="submit" value="" name="submit" class="btn btn-primary" style="width: 30%"></button>-->
                        <button class="btn btn-primary" style="width: 30%" type="submit">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>

        <pre>
        <?php
        include_once(__DIR__ . "/db.php");
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            if ($_GET !== []) {
                print_r($_GET);
                if ($_GET["visitorFirstName"] !== "") {
                    InsertVisitor($_GET);
                }
            }
        }
        ?>
        </pre>
    </main>

    <footer class="text-muted">
        <div class="container">
        </div>
    </footer>

</body>

</html>